import Token from '../Token';
import { ExpressionNode } from './ExpressionNode';
export default class ColumnNode {
    column: Token[]


    constructor(column: Token[]) {
        this.column = column;
    }
}