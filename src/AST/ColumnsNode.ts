import Token from '../Token';
import ColumnNode from './ColumnNode';
import { ExpressionNode } from './ExpressionNode';
import StarNode from './StarNode';
export default class ColumnsNode {
    star?: StarNode;
    columns?: ColumnNode[];


    constructor(star?: StarNode, columns?: ColumnNode[]) {
        this.star = star
        this.columns = columns
    }
}