import Token from '../Token';
import { ExpressionNode } from './ExpressionNode';
import LeftConditionNode from './LeftConditionNode';
import RightConditionNode from './RightConditionNode';

export default class ConditionsNode extends ExpressionNode {
    operator: Token;
    leftNode: LeftConditionNode;
    rightNode: RightConditionNode;


    constructor(operator: Token, leftNode: LeftConditionNode, rightNode: RightConditionNode) {
        super()
        this.operator = operator;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }
}