import Token from '../Token';
import TableNode from './TableNode';

export default class FromNode {
    type: Token;
    table: TableNode;


    constructor( type: Token, table: TableNode) {
        this.type = type;
        this.table = table;
    }
}