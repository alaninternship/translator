import Token from "../Token";
import SelectNode from "./SelectNode";

export default class QueryNode {
    type: Token;
    value: SelectNode
    
    constructor(type: Token){
        this.type = type;
    }
}