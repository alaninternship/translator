import Token from '../Token';
import ColumnsNode from './ColumnsNode';
import FromNode from './FromNode';
import StarNode from './StarNode';
import WhereNode from './WhereNode';

export default class SelectNode {
    type: Token;
    resultColums: ColumnsNode;
    from: FromNode;
    where: WhereNode;


    constructor(type: Token){
        this.type = type;
    }

}