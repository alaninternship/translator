import Token from "../Token";

export default class StarNode {
    type: Token;

    constructor( type: Token){
        this.type = type;
    }
}