import Token from "../Token";

export default class TableNode {
    type: Token

    constructor(type: Token){
        this.type = type
    }
}