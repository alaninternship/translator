import Token from '../Token';
import ConditionsNode from './ConditionsNode';
import { ExpressionNode } from './ExpressionNode';

export default class WhereNode {
    type: Token;
    conditions: ConditionsNode


    constructor(type: Token) {
        this.type = type;
    }
}