import SelectNode from "./AST/SelectNode";
import Token from "./Token";
import TokenType, { tokenTypesList } from "./TokenType";
import {ExpressionNode} from './AST/ExpressionNode'
import StatementsNode from "./AST/StatementsNode";
import ColumnsNode from "./AST/ColumnsNode";
import ColumnNode from "./AST/ColumnNode";

export default class Lexer {
    
    tokens: Token[];
    pos: number = 0;
    scope: any = {};

    constructor(tokens: Token[]) {
        this.tokens = tokens;
    }


    match(...expected: TokenType[]): Token | null {
        if (this.pos < this.tokens.length) {
            const currentToken = this.tokens[this.pos];
            if (expected.find(type => type.name === currentToken.type.name)){
                this.pos += 1;
                return currentToken;
                
            }
        }
        return null;
    }

    require(...expected: TokenType[]) {
        const token = this.match(...expected);
        if (!token) {
            throw new Error(`На позиции ${this.pos} ожидается ${expected[0].name}`)
        }
        if (!token) {
            throw new Error(`На позиции ${this.pos} ожидается ${expected[3].name}`)
        }
        if (!token) {
            throw new Error(`На позиции ${this.pos} ожидается ${expected[4].name}`)
        }

        return token;
    }


    parseProgram(): void {
        if (this.match(tokenTypesList.SELECT, tokenTypesList.FROM, tokenTypesList.WHERE) == null){
            throw new Error('ERRRRRRRORRORROROROROR!!!!!!!!!!!!!!!!!!!1')
        }
        console.log(this.match(tokenTypesList.SELECT, tokenTypesList.FROM, tokenTypesList.WHERE))
    }

    // parseColumns(): ExpressionNode {
    //     let leftColumn = this.parseIdentifier();
    //     if(leftColumn === this.match(tokenTypesList.STAR)){
    //         return new ColumnNode(leftColumn)
    //     }
    //     let comma = this.match(tokenTypesList.COMMA);
    //     while (comma != null ) {
    //         let rightColumn = this.parseIdentifier();
    //         leftColumn = new ColumnsNode(comma, leftColumn, rightColumn);
    //         comma = this.match(tokenTypesList.COMMA);
    //     }
    // }
    // parseIdentifier(): ExpressionNode {
    //     const column = this.match(tokenTypesList.IDENTIFIER);
    //     if(column != null){
    //         return new ColumnNode(column)
    //     }
    //     throw new Error(`Ожидается IDENTIFIER на ${this.pos} позиции`)
    // }

    // parseTokens(): ExpressionNode {
    //     const root = new StatementsNode();
    //     while (this.pos < this.tokens.length) {
    //         const codeStringNode = this.parseExpression();
    //         this.require(tokenTypesList.END);
    //         root.addNode(codeStringNode)
    //     }
    //     return root
    // }
}