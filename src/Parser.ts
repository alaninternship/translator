import Token from "./Token";
import {tokenTypesList} from "./TokenType";


export default class Parser {
    program: string;
    tokenList: Token[] = []
    cursor: {
        line: number,
        symbol: string,
        position: number
    }
    arrWithSymbols: string[];
    arrWithKeyWords: string[];
    tmpStrSymbols: string = "";
    tmpStrNumbers: string = "";
    tmpStrWords: string = ""; 
    constructor(program: string) {
        this.program = program;
        this.cursor = {
            line: 0,
            symbol: '',
            position: 0
        };
        this.arrWithSymbols = [",", "(", ")", "<", ">", ' ', '=', '<=', '>=', '==', '*', '+', '-', '!=', ';', '\n'];
        this.arrWithKeyWords = ['select', 'from', 'where']
    }
    

    parseQuery() {
        const arrWithParsedQuery = [];

        for( this.cursor.position; this.cursor.position < this.program.length; this.cursor.position++) {
          this.metrics()
          if(this.isNumber(this.program[this.cursor.position], this.program[this.cursor.position - 1], this.program[this.cursor.position + 1])){
            this.parseNumber(this.program[this.cursor.position], this.program[this.cursor.position - 1], this.program[this.cursor.position + 1], arrWithParsedQuery)
            continue
          }
          if(this.isStopsymbol(this.program[this.cursor.position])){
            this.parseStopsymbol(this.program[this.cursor.position], arrWithParsedQuery)
            continue
          }
          
          if(this.isWord(this.program[this.cursor.position])){
            this.parseWord(this.program[this.cursor.position], arrWithParsedQuery)
            continue
          }

          throw new Error('Неизвестный символ')
        }
        return arrWithParsedQuery;
    }

    metrics():void{
      this.cursor.symbol = this.program[this.cursor.position];
      if(this.cursor.symbol === '\n'){
      this.cursor.line += 1;
      }
    }

    isStopsymbol(symbol: string) : boolean { 
      if (this.arrWithSymbols.includes(symbol)) {
        return true
      }
      return false
    }

    isNumber(symbol : string, pastSymbol, nextSymbol) : boolean {
      const pastSymbolToNumber = parseInt(pastSymbol);
      const nextSymbolToNumber = parseInt(nextSymbol);
      const parsedSymbol = parseInt(symbol)
      if(isNaN(pastSymbolToNumber) === true && symbol === '.'){
        throw new Error(`Введено неверное число, нет цифр перед ${symbol}`)
      }
      if(isNaN(nextSymbolToNumber) === true && symbol === '.'){
        throw new Error(`Введено неверное число, нет цифр после ${symbol}`)
      }
      if( !isNaN(parsedSymbol) || symbol === '.'){
        return true
      }
      if( isNaN(pastSymbolToNumber) === true && isNaN(nextSymbolToNumber) === false && symbol === '-'){
        return true
      }
      
      return false     
    }

    isWord(symbol : string) : boolean {
      const parsedSymbol = parseInt(symbol)
      if (!isNaN(parsedSymbol)){
        return false
      }
      if (symbol === '_'){
      return true
      }
      return true
    }

    parseStopsymbol(symbol, arrWithParsedQuery){
        if( symbol === ' ' || symbol == '\n'){
            this.tmpStrWords ? arrWithParsedQuery.push(this.tmpStrWords) : null;
            this.tmpStrNumbers ? arrWithParsedQuery.push(this.tmpStrNumbers) : null;         
            this.tmpStrSymbols = "";
            this.tmpStrNumbers = '';
            this.tmpStrWords = '';
        }
        this. tmpStrWords ? arrWithParsedQuery.push(this.tmpStrWords) : null;
        this. tmpStrNumbers ? arrWithParsedQuery.push(this.tmpStrNumbers) : null;
        arrWithParsedQuery.push(symbol)         
        this. tmpStrSymbols = "";
        this. tmpStrNumbers = '';
        this.tmpStrWords = '';
    }

    parseNumber(symbol, pastSymbol, nextSymbol, arrWithParsedQuery){
        this.tmpStrWords ? arrWithParsedQuery.push(this.tmpStrWords) : null;
        this.tmpStrNumbers += symbol;

    }

    
    parseWord(symbol, arrWithParsedQuery){
        this.tmpStrNumbers ? arrWithParsedQuery.push(this.tmpStrNumbers) : null;
        this.tmpStrWords += symbol;
    }


    tokenize(){
      let code = this.parseQuery()
      const tokenTypesValues = Object.values(tokenTypesList);
      for(let i = 0; i < code.length; i++){
        this.setToken(tokenTypesValues, code, i)
      }
      console.log(this.tokenList)
      return this.tokenList
    }

    setToken(tokenTypesValues, code, i):void {
      for(let j = 0; j < tokenTypesValues.length; j++){
        if(code[0].toLowerCase() !== 'select'){
          throw new Error(`На позиции ${this.cursor.position} обнаружена ошибка, ожидается SELECT`)
          }
        if(tokenTypesValues[j].regex.test(code[i])){
          this.cursor.position += code[i].length
          const token = new Token(tokenTypesValues[j], code[i], this.cursor.position)
          this.tokenList.push(token)
          
          break
        }
        continue
      } 
    }

}
