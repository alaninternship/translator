export default class TokenType {
    name: string;
    regex: any;

    constructor(name: string, regex: any) {
        this.name = name;
        this.regex = regex;
    }
}



export const tokenTypesList = {
    SELECT: new TokenType('SELECT',/^[Ss][Ee][Ll][Ee][Cc][Tt]$/),
    AND: new TokenType('AND',/^[Aa][Nn][Dd]$/),
    AS: new TokenType('AS',/^[Aa][Ss]$/),
    FROM: new TokenType('FROM',/^[Ff][Rr][Oo][Mm]$/),
    WHERE: new TokenType('WHERE',/^[Ww][Hh][Ee][Rr][Ee]$/),
    TRUE: new TokenType('TRUE',/^[Tt][Rr][Uu][Ee]$/),
    FALSE: new TokenType('FALSE',/^[Ff][Aa][Ll][Ss][Ee]$/),
    NULL: new TokenType('NULL',/^[Nn][Uu][Ll][Ll]$/),
    IDENTIFIER: new TokenType('IDENTIFIER',/^[a-zA-Z_][a-zA-Z_0-9]*$/),
    LPAR: new TokenType('LPAR', /^\($/),
    RPAR: new TokenType('RPAR', /^\)$/),
    END:  new TokenType('END', /^\;$/),
    COMMA:  new TokenType('COMMA',/^\,$/),
    ASSIGN: new TokenType('ASSIGN',/^\=$/),
    STAR: new TokenType('STAR',/^\*$/),
    PLUS: new TokenType('PLUS',/^\+$/),
    MINUS: new TokenType('MINUS',/^\-$/),
    LT: new TokenType('LT',/^\<$/),
    LT_EQ: new TokenType('LT_EQ',/^\<=$/),
    GT: new TokenType('GT',/^\>$/),
    GT_EQ: new TokenType('GT_EQ',/^\>=$/),
    EQ: new TokenType('EQ',/^\==$/),
    NOT_EQ1: new TokenType('NOT_EQ1',/^\!=$/),
    DOT: new TokenType('DOT', /^\.$/),
    NUMERIC_LITERAL: new TokenType('NUMERIC_LITERAL',/^\-[0-9]+(\.[0-9][0-9]?)?$/),
    STRING_LITERAL: new TokenType('STRING_LITERAL',/^\.*$/),

}