import Lexer from "./Lexer";
import Parser from "./Parser";
import { tokenTypesList } from "./TokenType";


const argv = process.argv.slice(2)
let program = argv.join(' ');



const parser = new Parser(program)
const tokenList = parser.tokenize()

const lexer = new Lexer(tokenList)
lexer.parseProgram()

// parser.parsAnalysis()

// console.log(parser.tokenList)